import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const route: Routes = [ 
  {
    path: 'paises',
    loadChildren: () => import('./paises/paises.module').then( m => m.PaisesModule)
  },
  {
    path: '**',
    redirectTo: 'paises'
  },
]

@NgModule({
  imports: [
    RouterModule.forRoot(route)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
