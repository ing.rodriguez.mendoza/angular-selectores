import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaisesService } from '../../services/paises.service';
import { Pais, PaisSmall } from '../../interfaces/pais';
import { switchMap,tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-selector-page',
  templateUrl: './selector-page.component.html',
  styleUrls: ['./selector-page.component.css']
})
export class SelectorPageComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
      region     : ['', Validators.required],
      pais       : ['', Validators.required],
      frontera   : ['', Validators.required],
  });

  constructor(private fb: FormBuilder, 
              private paisesService: PaisesService) { }

  //selectores
  regiones: string[] = [];
  paises: Pais[] = [];
  fronteras: PaisSmall[] = [];
  cargando: boolean = false;

  ngOnInit(): void {

    //deshabilitar los controles
    this.miFormulario.get('pais')?.disable();
    this.miFormulario.get('frontera')?.disable();

    //cargamos las regiones
    this.regiones = this.paisesService.regiones;

    //obtenemos el valor seleccionado de la region
    this.miFormulario.get('region')?.valueChanges
    .pipe( 
        tap( () => {
          this.cargando = true;
          this.miFormulario.get('pais')?.reset('');
          this.miFormulario.get('frontera')?.disable();
        } ),
        switchMap( region => this.paisesService.getPaisesPorRegion(region) ), 
      )
    .subscribe(
      resp => {
        this.paises = resp;
        //habilitar el control
        this.miFormulario.get('pais')?.enable();    
        this.cargando = false;
        console.log('this.paises', this.paises);
      }
    );

    //obtenemos el valor seleccionado del pais
    this.miFormulario.get('pais')?.valueChanges
    .pipe(
      tap( (pais) => {
            this.cargando = true;
            this.fronteras = [];
            //this.miFormulario.get('frontera')?.reset('');
            console.log('pais', pais);
            this.miFormulario.get('frontera')?.disable();    

          }),
      switchMap( codigo => this.paisesService.getPaisPorCodigo( codigo ) ),
      switchMap( pais => this.paisesService.getPaisesPorCodigos( pais?.borders! ) )
    )
    .subscribe(
      resp => {
        // console.log('resp getPaisPorCodigo', resp);
        if (!resp) {
          return;
        }
        console.log('getPaisesPorCodigos', resp);
        // this.fronteras = resp?.borders || [];
        // if (this.fronteras.length > 0) {
        //   this.miFormulario.get('frontera')?.enable();    
        //   this.cargando = false;
        // } 
        this.fronteras = resp || [];
        this.miFormulario.get('frontera')?.enable();    
        this.cargando = false;


       // console.log('this.fronteras', this.fronteras);
      }
    );
    
  }

  guardar(){
    console.log(this.miFormulario.value);
  }

}
