import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { combineLatest, Observable, of } from 'rxjs';
import { Pais, PaisSmall } from '../interfaces/pais';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  //lo hacemos privado para impedir que se realicen modificacioni al objeto _regiones
  private _regiones: string[] = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];
  private _url: string = environment.url;

  constructor(private http: HttpClient) { }
  
  get regiones() : string[] {
    return [...this._regiones];//destructuracion de objetos[] sacamos una copia del objeto this._regiones con el operador (...)
  }

  getPaisesPorRegion(region: string): Observable<Pais[]>{
    return this.http.get<Pais[]>(`${this._url}/region/${region}`);
  }

  getPaisPorCodigo(code: string): Observable<PaisSmall | null> {
    if ( !code ) {
      return of(null)
    }
    return this.http.get<PaisSmall>(`${this._url}/alpha/${code}?fields=subregion&fields=region&fields=cca3&fields=borders`);
  }

  getPaisSmallPorCodigo(code: string): Observable<PaisSmall> {
    return this.http.get<PaisSmall>(`${this._url}/alpha/${code}?fields=subregion&fields=region&fields=cca3&fields=borders&fields=name`);
  }

  getPaisesPorCodigos(border: string[]): Observable<PaisSmall[]> {
    if ( !border ) {
      return of([])
    }

    const peticiones : Observable<PaisSmall>[] = [];

    border.forEach(
      codigo => {
        const peticion = this.getPaisSmallPorCodigo(codigo);
        peticiones.push(peticion);
      }
    );
    //retorna un observable que contiene un arreglo con el producto de todas las peticiones internas
    return combineLatest( peticiones );
  }
  

}
